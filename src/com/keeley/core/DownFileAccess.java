package com.keeley.core;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Created with IntelliJ IDEA. User: Administrator Date: 14-7-29 Time: 下午2:15 To
 * change this template use File | Settings | File Templates.
 */
public class DownFileAccess {
	/**
	 *
	 */
	private static final long serialVersionUID = -2518013155676212866L;
	// 写入文件的流
	RandomAccessFile oSavedFile;
	// 开始位置
	long nPos;
	boolean bFirst;

	public DownFileAccess() throws IOException {
		this("", 0, true);
	}

	/**
	 * 写入文件初始化
	 * 
	 * @param sName
	 * @param nPos
	 * @throws IOException
	 */
	public DownFileAccess(String sName, long nPos, boolean bFirst) throws IOException {
		File wfile = new File(sName);
		//设置隐藏
		String sets = "attrib +H \"" + wfile.getAbsolutePath() + "\"";
		// 输出命令串
		System.out.println(sets);
		// 运行命令串
		Runtime.getRuntime().exec(sets);
		oSavedFile = new RandomAccessFile(wfile, "rw");
		if (!bFirst) {
			oSavedFile.seek(wfile.length());
		}
		this.nPos = nPos;
		this.bFirst = bFirst;
	}

	/**
	 * 写文件
	 * 
	 * @param b
	 * @param nStart
	 * @param nLen
	 * @return
	 */
	public synchronized int write(byte[] b, int nStart, int nLen) {
		int n = -1;
		try {
			oSavedFile.write(b, nStart, nLen);
			n = nLen;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return n;
	}
}
